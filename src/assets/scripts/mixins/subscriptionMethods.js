/* eslint-disable radix */
/* eslint-disable promise/always-return */
/* eslint-disable eqeqeq */
/**
 * Custom methods
 * -----------------------------------------------------------------------------
 *
 * @namespace subscriptionMethods
 */
import Vue from 'vue';
// media queries in Javascript => http://wicky.nillia.ms/enquire.js/
import enquire from 'enquire.js';
// detect if a device is mouseOnly, touchOnly, or hybrid => http://detect-it.rafrex.com/
import detectIt from 'detect-it';
import scrollMonitor from 'scrollmonitor';

// date picker component
import flatPickr from 'vue-flatpickr-component';
import 'flatpickr/dist/flatpickr.css';
import axios from 'axios';
import clientShopify from '../api/clientShopify';
import store from '../store/store';

const subscriptionMethods = {

    components: {
        flatPickr,
    },

    data() {
        return {

        };
    },
    methods: {
    _initSubscribeMethods(){
      // custom JS code
        const $container = $(app.selectors.subscribeFunnel);
        const $ProductContainer = $(app.selectors.productContainer);
        // console.log($ProductContainer);
        const redirectPage = localStorage.getItem('redirectPage');
        const gift_enabled = localStorage.getItem('gift_enabled');
        if (app._getUrlParameter('gift') == 1){
            app.isGiftcartForm = false;
        }
        if (app._getUrlParameter('gift') == 1 && gift_enabled == 'true'){
            app.isGiftcartForm = false;
            const subscription_plan = localStorage.getItem('subscription_plan');
            const giftplanProductJson = JSON.parse(subscription_plan);
            app._getCurrentPickProduct(giftplanProductJson.value);
            app.isPickPlan = true;
            setTimeout(() => {
                app._currentStage(2);
            });
        }
        if (app._getUrlParameter('handle') == 'monthly-plans'){
            const planOfSub = $(`#collection_${app._getUrlParameter('handle')}`).html();
            const currentPick = {};
            if (planOfSub.length > 0){
                const pickplanJson = JSON.parse(planOfSub);
                currentPick.value = pickplanJson.handle;
                currentPick.title = pickplanJson.title;
                currentPick.id = pickplanJson.id;
                app.funnel.pickPlan = currentPick; // Pick Plan
                const prodDetails = $(`#${app._getUrlParameter('handle')}`).html();
                const planProductJson = JSON.parse(prodDetails);
                app.funnel.product = planProductJson;
                app.isPickPlan = true;
                setTimeout(() => {
                    app._nextStage(1, 2);
                }, 100);
            }
        }
        app.customer.email = $('[data-customer-email]').val();
        app.customer.id = $('[data-customer-id]').val();

        const $weekContainer = $(app.selectors.subscribeWeeklyPlan);
        const currentPick = {};
        if ($weekContainer.length > 0){
            const prodMeta = $('[data-collection-subscribe-json]').html();
            const pickplanJson = JSON.parse(prodMeta);
            currentPick.value = pickplanJson.handle;
            currentPick.title = pickplanJson.title;
            currentPick.id = pickplanJson.id;
            app.funnel.pickPlan = currentPick; // Pick Plan

            const prodDetails = $('[data-product-subscribe-json]').html();
            // console.log($('[data-product-subscribe-json]').html());
            const planProductJson = JSON.parse(prodDetails);
            app.funnel.product = planProductJson;

        }


        if (redirectPage == 'true') {
            const currentStep = localStorage.getItem('current_step');
            $('[data-stage="2"]').removeClass('active');
            /* Stage 1 */
            app.isPickPlan = true;
            const currentPick = localStorage.getItem('subscription_plan');
            if (currentPick){
                app.funnel.pickPlan = JSON.parse(currentPick);
                app._getCurrentPickProduct(app.funnel.pickPlan.value);
            }
            /* Stage 2 */
            app.isChoosePlan = true;
            const responsedata = localStorage.getItem('selected_plan');
            app.funnel.choosePlan = JSON.parse(responsedata);

            /* Stage 3 */
            if (currentStep == '4'){
                app.isPayment = true;
                const selected_payment = localStorage.getItem('selected_payment');
                app.funnel.payment = JSON.parse(selected_payment);
                setTimeout(() => {
                    app._subscriptionDatePickerDelivary();
                });
            }

            setTimeout(() => {
                app._currentStage(currentStep);
            });


        }

        if (window.location.href.indexOf('/pages/subscribe') > 0 || window.location.href.indexOf('/pages/weekly-plans') > 0) {
            localStorage.setItem('redirectPage', null);
        }


    },

    _nextStage(current_step, nextStep){
        localStorage.setItem('current_step', nextStep);
        localStorage.setItem('redirectPage', null);
        switch (nextStep) {
            case 2:
                app.isPickPlan = true;
                localStorage.setItem('subscription_plan', JSON.stringify(app.funnel.pickPlan));
                app._getCurrentPickProduct(app.funnel.pickPlan.value);
                // $('html').animate({
                //     scrollTop: $('body').offset().top,
                // }, 400);
                break;
            case 3:
                app.isChoosePlan = true;
                // $('html').animate({
                //     scrollTop: $('body').offset().top,
                // }, 400);
                break;
            case 4:
                app.isPayment = true;
                localStorage.setItem('redirectPage', null);
                if (app.funnel.payment != ''){
                    localStorage.setItem('selected_payment', JSON.stringify(app.funnel.payment));
                }
                // $('html').animate({
                //     scrollTop: $('body').offset().top,
                // }, 400);
                setTimeout(() => {
                    app._subscriptionDatePickerDelivary();
                });
                break;
        }

        $(`[data-step="${nextStep}"]`).removeClass('d-none');
        $(`[data-step="${current_step}"]`).addClass('d-none');
        $(`[data-stage="${current_step}"]`).removeClass('active');
        $(`[data-stage="${nextStep}"]`).addClass('active').removeClass('disable');
    },

    _prevStage(event, current_step, prevStep){
        localStorage.setItem('current_step', prevStep);
        localStorage.setItem('redirectPage', null);
        $(`[data-step="${prevStep}"]`).removeClass('d-none');
        $(`[data-step="${current_step}"]`).addClass('d-none');
        $(`[data-stage="${current_step}"]`).removeClass('active').addClass('disable');
        $(`[data-stage="${prevStep}"]`).addClass('active').removeClass('disable');

        switch (prevStep) {
            case 1:
                app.isPickPlan = false; // Step 1
                $('[name="pick-plan"]').prop('checked', false); // Step 1
                app.isChoosePlan = false; // Step 2
                //localStorage.setItem('subscription_plan', null); // Step 2
                app.isPayment = false; // Step 3
                localStorage.setItem('selected_plan', null); // Step 3
                $('[data-stage="2"]').addClass('active').removeClass('disable');
                $('[data-stage="3"]').removeClass('active').addClass('disable');
                break;
            case 2:
                app.isChoosePlan = false; // Step 2
                localStorage.setItem('subscription_plan', null); // Step 2
                app.isPayment = false; // Step 3
                localStorage.setItem('selected_plan', null); // Step 3
                $('[data-stage="3"]').removeClass('active').addClass('disable');
                break;
            case 3:
                app.isPayment = false;
                localStorage.setItem('selected_plan', null);
                break;
            case 4:
                break;
        }
    },

    _currentStage(current_step){
        $('.funnelSection').addClass('d-none');
        $(`[data-stage="${current_step}"]`).addClass('active').removeClass('disable');
        $(`[data-step="${current_step}"]`).removeClass('d-none');
    },

    _editStage(event, editStage, current_step){
        localStorage.setItem('current_step', editStage);
        $(`[data-step="${editStage}"]`).removeClass('d-none');
        $(`[data-step="${current_step}"]`).addClass('d-none');
        $(`[data-stage="${editStage}"]`).addClass('active').removeClass('disable');
        $(`[data-stage="${current_step}"]`).removeClass('active').addClass('disable');
        if (editStage == 2){
            $('[data-stage="3"]').removeClass('active').addClass('disable');
        }
    },

    _getPickPlan(){
        let $container;
        const singleOptionSelector = app.selectors.subscribePickPlan;
        $.map($(singleOptionSelector, $container), (element) => {
            const $element = $(element);
            console.log($element);
            const type = $element.attr('type');
            const currentPick = {};

            if (type === 'radio' || type === 'checkbox') {
                if ($element[0].checked) {
                    currentPick.value = $element.val();
                    currentPick.title = $element.data('title');
                    currentPick.id = $element.data('id');
                    app.funnel.pickPlan = currentPick;
                    currentPick.link = $element.data('href');
                    // console.log('===', currentPick);
                    if (currentPick.link != undefined){
                        window.location.replace(currentPick.link);
                    }
                    setTimeout(() => {
                        app._nextStage(1, 2);
                    }, 100);
                    return currentPick;
                }
                return false;
            }
            currentPick.value = $element.val();
            currentPick.index = $element.data('title');
            currentPick.id = $element.data('id');
            app.funnel.pickPlan = currentPick;
            setTimeout(() => {
                app._nextStage(1, 2);
            }, 100);
            return currentPick;
        });

    },

    _getPickPlanOnGift(handle){
        const $element = $(event.currentTarget);
        const currentPick = {};
        currentPick.value = handle;
        currentPick.title = $element.data('title');
        currentPick.id = $element.data('id');
        app.funnel.pickPlan = currentPick; // Pick Plan
        localStorage.setItem('subscription_plan', JSON.stringify(app.funnel.pickPlan));
        localStorage.setItem('gift_enabled', true);
        if ($element.attr('href') != undefined){
            window.location.replace($element.attr('href'));
        }
    },
    _getCurrentPickProduct(collection){
      // console.log('======', collection);
        const addonIds = JSON.parse($(`#${collection}`).html());
        console.log(addonIds);
        app.funnel.product = addonIds;
        $.each(addonIds, (index, value) => {
          // console.log('value====', value);
            const prodMeta = $(`[data-${value.handle}-json]`).html();
            Vue.set(app.funnel.product[index], 'short_desc', prodMeta);

            $.each(value.tags, (indexTags, valueOfTag) => {
            //   console.log('valuetag', value.tags);

                if (valueOfTag.indexOf('filter-monthly') !== -1) {
                    Vue.set(app.funnel.product[index], 'filter', 'mo');
                    return false;
                }
                if (valueOfTag.indexOf('filter-weekly') !== -1) {
                    Vue.set(app.funnel.product[index], 'filter', 'week');
                    return false;
                }
                if (valueOfTag.indexOf('filter-bi-weekly') !== -1) {
                    Vue.set(app.funnel.product[index], 'filter', 'bi-weekly');
                    return false;
                }
            });
        });
    },

    _pickplanslider(){
        $('[data-pickplan-slider]').slick({
            responsive: [
                {
                    breakpoint: 1200,
                    settings: {
                    slidesToShow: 3,
                    },
                },
                {
                    breakpoint: 768,
                    settings: {
                    slidesToShow: 2,
                    },
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                    },
                },
            ],
        });
    },

    _choosePlan(handle){
        axios.get(`/products/${handle}.js`)
        .then((response) => {
            console.log(response);
            app.funnel.choosePlan = response.data;
            localStorage.setItem('selected_plan', JSON.stringify(app.funnel.choosePlan));
            const mainPrice = app.funnel.choosePlan.price;
            app._addedCustomAttrInProduct(app.funnel.choosePlan, mainPrice);
        })
        .catch((error) => {
        // console.log(error)
        });
        setTimeout(() => {
            app._nextStage(2, 3);
        }, 100);
    },

    _chooseWeeklyPlan(handle){
        axios.get(`/products/${handle}.js`)
        .then((response) => {
            app.funnel.choosePlan = response.data;
            setTimeout(() => {
                $.each(app.funnel.choosePlan.variants, (index, value) => {
                    if (value.is_hidden == false){
                        app.funnel.payment = value;
                    }
                });
            });

            const mainPrice = app.funnel.choosePlan.price;
            app._addedCustomAttrInProduct(app.funnel.choosePlan, mainPrice);
        })
        .catch((error) => {
         console.log(error);
        });
        setTimeout(() => {
            app._nextStage(2, 4);
        }, 500);
    },

    _addedCustomAttrInProduct(product, mainPrice){
        // console.log(product);
        $.each(product.variants, (index, variant) => {
            const chargeInternalFRQ = $(`[data-${variant.id}-charge-frq-json]`).html(); 
            // console.log($(`[data-${variant.id}-charge-frq-json]`).html());
            const is_hidden = $(`[data-${variant.id}-visible-json]`).html();
            // console.log($(`[data-${variant.id}-visible-json]`).html());
            const priceOfEach = variant.price;
            if (is_hidden != undefined){
                Vue.set(variant, 'is_hidden', $.parseJSON($(`[data-${variant.id}-visible-json]`).html()));
            }
            if (chargeInternalFRQ != undefined){
                Vue.set(variant, 'charge_interval_frequency', $.parseJSON($(`[data-${variant.id}-charge-frq-json]`).html()));
                Vue.set(variant, 'each_month', parseInt(priceOfEach / $.parseJSON($(`[data-${variant.id}-charge-frq-json]`).html())));
                /* Save Price calculate */
                const diffPrice = parseInt(mainPrice - variant.each_month);
                Vue.set(variant, 'save', parseInt(diffPrice * $.parseJSON($(`[data-${variant.id}-charge-frq-json]`).html())));

            }
        });

        /* Subscription Properties in Elemenet */

        if ($(`[data-${product.handle}-subscribe-json]`).length > 0){
            // console.log('--------');
            const subscriptionMeta = $.parseJSON($(`[data-${product.handle}-subscribe-json]`).html());

            // console.log(subscriptionMeta);
            // console.log('--------');
            if (subscriptionMeta[0].has_subscription != '' && subscriptionMeta[0].has_subscription != undefined){
                Vue.set(product, 'has_subscription', subscriptionMeta[0].has_subscription);
            }
            if (subscriptionMeta[0].is_subscription_only != '' && subscriptionMeta[0].is_subscription_only != undefined){
                Vue.set(product, 'is_subscription_only', subscriptionMeta[0].is_subscription_only);
            }
            if (subscriptionMeta[0].shipping_interval_frequency != '' && subscriptionMeta[0].shipping_interval_frequency != undefined){
                Vue.set(product, 'shipping_interval_frequency', subscriptionMeta[0].shipping_interval_frequency);
            }
            if (subscriptionMeta[0].shipping_interval_unit_type != '' && subscriptionMeta[0].shipping_interval_unit_type != undefined){
                Vue.set(product, 'shipping_interval_unit_type', subscriptionMeta[0].shipping_interval_unit_type);
            }
            if (subscriptionMeta[0].subscription_id != '' && subscriptionMeta[0].subscription_id != undefined){
                Vue.set(product, 'subscription_id', subscriptionMeta[0].subscription_id);
            }
        }
        if (app.currentTemplate !== 'account') {
            $.each(product.tags, (index, value) => {
                console.log(value);
                if (value.indexOf('filter-monthly') !== -1) {
                    Vue.set(product, 'filter', 'mo');
                    return false;
                }
                if (value.indexOf('filter-weekly') !== -1) {
                    Vue.set(product, 'filter', 'week');
                    return false;
                }
                if (value.indexOf('filter-bi-weekly') !== -1) {
                    Vue.set(product, 'filter', 'bi-weekly');
                    return false;
                }
            });
        }
    },

    _chooseProductOnPage(handle, step){
        localStorage.setItem('redirectPage', true);
        app.funnel.choosePlan = app.product;
        const mainPrice = app.funnel.choosePlan.price;
        app._addedCustomAttrInProduct(app.funnel.choosePlan, mainPrice);
        localStorage.setItem('current_step', 3);
        const collJson = JSON.parse($('[data-collection-subscribe-json]').html());
        const currentPick = {};
        currentPick.value = collJson.handle;
        currentPick.title = collJson.title;
        currentPick.id = collJson.id;
        app.funnel.pickPlan = currentPick;
        localStorage.setItem('subscription_plan', JSON.stringify(app.funnel.pickPlan));
        localStorage.setItem('selected_plan', JSON.stringify(app.funnel.choosePlan));
        window.location.replace('https://bloomsybox.myshopify.com/pages/subscribe');

    },

    _chooseProductWeeklyPage(handle, step){
        localStorage.setItem('redirectPage', true);
        app.funnel.choosePlan = app.product;
        const mainPrice = app.funnel.choosePlan.price;
        app._addedCustomAttrInProduct(app.funnel.choosePlan, mainPrice);
        localStorage.setItem('current_step', 4);
        const collJson = JSON.parse($('[data-collection-subscribe-json]').html());
        const currentPick = {};
        currentPick.value = collJson.handle;
        currentPick.title = collJson.title;
        currentPick.id = collJson.id;
        app.funnel.pickPlan = currentPick;
        localStorage.setItem('subscription_plan', JSON.stringify(app.funnel.pickPlan));
        localStorage.setItem('selected_plan', JSON.stringify(app.funnel.choosePlan));
        localStorage.setItem('selected_payment', JSON.stringify(app.currentVariant));
        window.location.replace('https://bloomsybox.myshopify.com/pages/weekly-plans');
    },

    _selectedPlan(variant){
        app.funnel.payment = variant;
        setTimeout(() => {
            app._nextStage(3, 4);
        }, 100);
    },


    _subscriptionDatePickerDelivary(){
        const selectionPlan = app.funnel.choosePlan.filter;

        const holidays = app.holidays;
        const holidaysDisabled = [
            function(date) {
                // must return true to disable
                return (date.getDay() === 6 || date.getDay() === 0 || date.getDay() === 1);
            },
        ];
        for (let i, e = 0, t = holidays; e < t.length; e += 1) {
            const values = {};
            values.from = t[e],
            values.to = t[e];
            holidaysDisabled.push(values);
        }
        flatpickr('#subScriptionDateConfig', {
            altFormat: 'M j, Y',
            altInput: true,
            mode: 'single',
            disableMobile: true,
            dateFormat: 'm-d-Y',
            enableTime: false,
            locale: {
                firstDayOfWeek: 1, // start week on Monday
            },
            onClose(selectedDates, dateStr, instance){
                $('.flatpickr-input').blur();
            },
            // minDate: "today",
            minDate: new Date().fp_incr(7), // 2 days from now
            // enable: [
            //     function(date) {
            //         // return true to disable
            //         return (date.getDate() === 4);
            //     },
            // ],

            // disable Saturdays and Sundays and Mondays
            // disable using a function
            disable: holidaysDisabled,
        });
    },

    /***************
     * Marth script
     *
     **********/

     _msSelectSubscribeProduct(event, handle){
        const productObj = $(`[data-${handle}-json]`).html();
        const productData = JSON.parse(productObj);
        app.marthaFunnel.choosePlan = productData;
        const product = productData.product;
        localStorage.setItem('selected_plan', JSON.stringify(app.marthaFunnel.choosePlan));
        const mainPrice = product.price;
        app._addedCustomAttrInProduct(product, mainPrice);
        setTimeout(() => {
            app._nextStage(2, 3);
        }, 100);
     },

     _msSelectedPlan(variant){
        app.marthaFunnel.payment = variant;
        setTimeout(() => {
            app._nextStage(3, 4);
        }, 100);
    },

  },
};

export default subscriptionMethods;
