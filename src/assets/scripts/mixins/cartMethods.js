/**
 * Cart methods
 * -----------------------------------------------------------------------------
 *
 * @namespace cartMethods
 */

// date picker component
import flatPickr from 'vue-flatpickr-component';
import 'flatpickr/dist/flatpickr.css';
import axios from 'axios';
import clientShopify from '../api/clientShopify';
import store from '../store/store';

const cartMethods = {
  components: {
    flatPickr,
  },

  data() {
    return {
      // promo products
      promoProducts: {},
      promosTotal: 0,
      cartTotal: 0,
      gwpEnablerCodes: [],

      // shipping calculator
      countries: window.Countries,
      provinces: null,
      selectedCountry: null,
      selectedProvince: null,
      selectedZip: null,
      zipRequired: false,
      shippingRates: null,
      shippingRatesErrors: null,

      // DeliveryDate
      // Get more from https://chmln.github.io/flatpickr/options/


      deliveryDateConfig: {
        wrap: true, // set wrap to true only when using 'input-group'
        altFormat: 'M j, Y',
        altInput: true,
        dateFormat: 'm-d-Y',
        enableTime: false,
        locale: {
          firstDayOfWeek: 1, // start week on Monday
        },
        // minDate: "today",
        minDate: new Date().fp_incr(2), // 2 days from now

        // disable Saturdays and Sundays and Mondays
        // disable using a function
        disable: [

          function(date) {
            // must return true to disable
            return (date.getDay() === 6 || date.getDay() === 0 || date.getDay() === 1);
          },
        ],
      },


      // date picker
      // Get more from https://chmln.github.io/flatpickr/options/
      datePickerConfig: {
        wrap: true, // set wrap to true only when using 'input-group'
        altFormat: 'M j, Y',
        altInput: true,
        dateFormat: 'm-d-Y',
        enableTime: false,
        locale: {
          firstDayOfWeek: 1, // start week on Monday
        },
        // minDate: "today",
        minDate: new Date().fp_incr(2), // 2 days from now

        // disable Saturdays and Sundays
        // disable using a function
        disable: [
          function(date) {
            // must return true to disable
            return (date.getDay() === 6 || date.getDay() === 0);
          },
        ],
      },
    };
  },

  watch: {
    selectedCountry() {
      const selectedCountryObj = app.countries[app.selectedCountry];
      const selectedProvincesObj = selectedCountryObj.province_alternate_names;
      const provincesLength = Object.keys(selectedProvincesObj).length;

      // clear data
      app.shippingRates = null;
      app.shippingRatesErrors = null;
      app.selectedZip = null;

      // set data
      app.zipRequired = selectedCountryObj.zip_required;

      if (provincesLength > 0) {
        app.provinces = selectedProvincesObj;
        // app.selectedProvince = Object.keys(app.provinces)[0]
      } else {
        app.provinces = null;
      }
    },
  },

  methods: {
    _initCart() {
      const container = app.selectors.siteHeader;
      const cartObj = app.selectors.cartObj;

      // GWP functionality
      app._setGwpEnablerCodes();
      app._datePickerDelivary();
      app._getCartData();
      // stop parsing if we don't have the cart json script tag when loading
      if (!$(cartObj, container).html()) {
        return;
      }

      // set the cart data in the global store object (Vuex)
      app.cart = JSON.parse($(cartObj, container).html());
      if (app.cart.attributes.To){
        app.isGiftcartForm = false;
      }

      // cart promo products
      app._initCartPromos();

      // shipping calculator
      app._initShippingCalculator();
    },

    /**
     * Get the cart data json object
     */
    /*_getCartData() {
      const config = {
        headers: {
          'Cache-Control': 'must-revalidate',
          'Cache-Control': 'no-cache',
          'Cache-Control': 'no-store',
          Pragma: 'no-cache',
        },
      };
      axios.get('/cart.js', config)
      .then((response) => {
        app.cart = response.data;
        app._initCartPromos();
        if (app.cart.item_count == 0) {
            axios.post('/cart/update.js', {
                attributes: {
                    To: '',
                    From: '',
                    Gift_Message: '',
                },
            });
          $('#productItemListing').remove();
          $('.emptyCartItem').removeClass('d-done').addClass('d-flex');
        }
      })
      .catch((error) => {
        // console.log(error)
      });
    },*/

    async _getCartData() {
        const config = {
          headers: {
            'Cache-Control': 'must-revalidate',
            'Cache-Control': 'no-cache',
            'Cache-Control': 'no-store',
            Pragma: 'no-cache',
          },
        };
  
        const cartData = await axios.get('/cart.js', config);
        app.cart = cartData.data;
  
        if (app.cart.item_count == 0) {
          await axios.post('/cart/update.js', {
              attributes: {
                  To: '',
                  From: '',
                  Gift_Message: '',
                  Delivery_Date: '',
              },
          });
  
          $('#productItemListing').remove();
          $('.emptyCartItem').removeClass('d-done').addClass('d-flex');
        }
    },

    /**
     * Event from quantity stepper to change the cart
     *
     * @param  {number} variantId - current variant id
     */

    _addToCartADDONS(event, variantId) {

        let container;

        if (app.isQuickshopModalOpen) {
          container = app.selectors.quickShopContainer;
        } else {
          container = app.selectors.productContainer;
        }
  
        const $qtyEl = $(event.currentTarget).parentsUntil(container).find(app.selectors.qtyInput);
        let qtyValue = $qtyEl.val();
  
        if (event === 'grid-item') { // event is from collection product grid item
          qtyValue = 1;
        }
  
        app.isLoading = true;
        app.loadingEvent = `cartAdding_${variantId}`;
        const id=[];
        axios.post('/cart/add.js', {
          quantity: qtyValue,
          id: variantId,
        })
        .then((response) => {
          app._getCartData();
          app._vibrateDevice();
          $qtyEl.val(1); 
          setTimeout(() => {
            app.isLoading = false;
            app.loadingEvent = null;
            app.isaddedTocart = true,
            app.isloadingEvent = `cartAdded_${variantId}`;
            $(`#cartAdded_${variantId}`).removeClass('d-none');
            $(`.cartAdded_${variantId}`).addClass('d-none');
            id.push(variantId);
            app.addedProduct = id;
          }, 500);
        })
        .catch((error) => {
          // console.log(error)
        });
    },

    _addToCartOnsPro(event, variantId) {
        const data = [];
        const values = {};
        const propValues = {};
        const propAttr = {};
        const dataObj = [];
        let container;

        if (app.isQuickshopModalOpen) {
            container = app.selectors.quickShopContainer;
        } else {
            container = app.selectors.productContainer;
        }


        const $qtyEl = $(event.currentTarget).parentsUntil(container).find(app.selectors.qtyInput);
        let qtyValue = $qtyEl.val();

        if (event === 'grid-item') { // event is from collection product grid item
          qtyValue = 1;
        }
        values.quantity = qtyValue;
        //console.log($(event.target).closest('[data-form-product]').serializeArray());
        $.each($(event.target).closest('[data-form-product]').serializeArray(), (i, field) => {
            const str1 = field.name;
            const strID = 'id';
            const str2 = 'properties';
            const str3 = 'attributes';
                if (str1.indexOf(strID) > -1 && field.value != ''){
                    const productID = field.value;
                    dataObj.push(productID);
                }
                if (str1.indexOf(str2) > -1 && field.value != ''){
                    let propertiesName = field.name;
                    propertiesName = propertiesName.replace('properties[', '');
                    propValues[propertiesName.replace(']', '')] = field.value;
                }
                if (str1.indexOf(str3) > -1 && field.value != ''){
                    let attrName = field.name;
                    attrName = attrName.replace('attributes[', '');
                    propAttr[attrName.replace(']', '')] = field.value;
                }
          });
            values.id = dataObj;
            values.properties = propValues;
            values.attributes = propAttr;
            data.push(values);
        //   console.log('data-->',data);
        app.isLoading = true;
        app.loadingEvent = 'cartAddingAdons';
        const lengthCart = data[0].id.length;
        axios.post('/cart/add.js', data[0])
        .then((response) => {
            // app.isCartDrawerAddOnsOpen = true;
            const recentlyCart = [];
            const addedId =  data[0].id;
            app._getCartData();
            app._vibrateDevice();
            $qtyEl.val(1); // reset qty input to 1
            $('[data-quickshop]').modal('hide');

            setTimeout(() => {
                    for (let i, e = 0, t = app.cart.items; e < t.length; e += 1) {
                      if(addedId.find(x => x == app.cart.items[e].id) != undefined){
                        if(recentlyCart.find(x => x.id == app.cart.items[e].id) == undefined){
                          recentlyCart.push(app.cart.items[e]);
                        }
                      }
                    }
                app.recentlyCart = recentlyCart;
            }, 500);
            setTimeout(() => {
                $('[name="id"]').prop('checked', false);
                app.isLoading = false;
                app.loadingEvent = null;
                app._toggleCartDrawerAddOns();
                
            }, 1000);
        })
        .catch((error) => {
            // console.log(error)
        });

    },

    /**
     * Subscription Flow
     * Event from quantity stepper to change the cart
     *
     * @param  {number} variantId - current variant id
     */
    async _addToCartSubscription(event, variantId) {
      let container;

      if (app.isQuickshopModalOpen) {
        container = app.selectors.quickShopContainer;
      } else {
        container = app.selectors.productContainer;
      }

      const $qtyEl = $(event.currentTarget).parentsUntil(container).find(app.selectors.qtyInput);
      let qtyValue = $qtyEl.val();

      if (event === 'grid-item') { // event is from collection product grid item
        qtyValue = 1;
      }

      const $gift = $(event.currentTarget).parentsUntil(container).find('[data-gift]');
      if ($gift[0].checked) {

        // const $to = $(event.currentTarget).parentsUntil(container).find('[data-name-to]');
        // const $from = $(event.currentTarget).parentsUntil(container).find('[data-name-from]');
        const $message = $(event.currentTarget).parentsUntil(container).find('[data-name-message]');

        //if ($to.val() != '' && $from.val() != '' && $message.val() != ''){
        if ($message.val() != ''){          
            // $to.removeClass('border-danger');
            // $from.removeClass('border-danger');
            app.isGiftMsg = false;
            $message.removeClass('border-danger');
        // } else if ($to.val() == ''){
        //     $to.addClass('border-danger');
        //     return false;
        // } else if ($from.val() == ''){
        //     $from.addClass('border-danger');
        //     return false;
        } else if ($message.val() == ''){
            $message.addClass('border-danger');
            app.isGiftMsg = true;
            if ($(window).width() > 991) {
              $('html, body').animate({scrollTop:0}, 'slow');
            } else {
              $('html, body').animate({scrollTop: $('[data-name-message]').offset().top - 120 }, 200);
            }
            $message.focus();
            return false;
        }

      } else {
        await axios.post('/cart/update.js', {
            attributes: {
                // To: '',
                // From: '',
                Gift_Message: '',
            },
        }).then((response) => {
            app._getCartData();
        })
        .catch((error) => {
            // console.log(error)
        });
      }

      const values = {};
      const propValues = {};
      const propAttr = {};
      const data = [];
      values.quantity = qtyValue;
      values.id = variantId;
      $.each($(event.target).closest('[data-form-product]').serializeArray(), (i, field) => {

        const str1 = field.name;
        const str2 = 'properties';
        const str3 = 'attributes';
          if (str1.indexOf(str2) > -1 && field.value != ''){
            let propertiesName = field.name;
            propertiesName = propertiesName.replace('properties[', '');
            propValues[propertiesName.replace(']', '')] = field.value;
          }
          if (str1.indexOf(str3) > -1 && field.value != ''){
            let attrName = field.name;
            attrName = attrName.replace('attributes[', '');
            propAttr[attrName.replace(']', '')] = field.value;
        }
      });
      values.properties = propValues;
      values.attributes = propAttr;
      data.push(values);
      app.isLoading = true;
      app.loadingEvent = 'cartAdding';
      
      await axios.post('/cart/add.js', data[0])
      .then((response) => {
        app._getCartData();
        app._vibrateDevice();
        $qtyEl.val(1); // reset qty input to 1
        $('[data-quickshop]').modal('hide');
        setTimeout(() => {
		  app._redirectToCheckout();
          app.isLoading = false;
          app.loadingEvent = null;

        }, 500);
      })
      .catch((error) => {
        // console.log(error)
      });
    },

    /**
     * Event from quantity stepper to change the cart
     *
     * @param  {number} variantId - current variant id
     */
    _addToCart(event, variantId) {
      let container;

      if (app.isQuickshopModalOpen) {
        container = app.selectors.quickShopContainer;
      } else {
        container = app.selectors.productContainer;
      }

      const $qtyEl = $(event.currentTarget).parentsUntil(container).find(app.selectors.qtyInput);
      let qtyValue = $qtyEl.val();

      if (event === 'grid-item') { // event is from collection product grid item
        qtyValue = 1;
      }

      app.isLoading = true;
      app.loadingEvent = 'cartAdding';

      axios.post('/cart/add.js', {
        quantity: qtyValue,
        id: variantId,
      })
      .then((response) => {
        app._getCartData();
        app._vibrateDevice();
        $qtyEl.val(1); // reset qty input to 1
        $('[data-quickshop]').modal('hide');

        setTimeout(() => {
          app.isLoading = false;
          app.loadingEvent = null;

          if (event !== 'promo-item') {
            app._toggleCartDrawer();
          }
        }, 500);
      })
      .catch((error) => {
        // console.log(error)
      });
    },

    /**
     * Event from quantity stepper to change the cart
     *
     * @param  {number} itemId - line item index
     */
    _removeFromCart(event, itemIndex) {
      const $cartItem = $(event.target).parentsUntil(app.selectors.cartItem).parent();

      app.isLoading = true;
      $cartItem.addClass('updating');

      axios.post('/cart/change.js', {
        quantity: 0,
        line: itemIndex,
      })
      .then((response) => {
		app.isLoading = false;
		window.location.href = window.location.href;
        //app._getCartData();
        $('[data-toggle="tooltip"]').tooltip('hide');
      })
      .catch((error) => {
        // console.log(error)
      });
    },

    /**
     * Event from quantity stepper to change the cart
     *
     * @param  {number} itemIndex - line item index
     */
    _updateCart(event, itemIndex) {
      const $qtyEl = $(event.currentTarget).parentsUntil(app.selectors.qtyContainer).find(app.selectors.qtyInput);
      const $cartItem = $(event.target).parentsUntil(app.selectors.cartItem).parent();
      let qtyValue = $qtyEl.val();

      app.isLoading = true;
      $cartItem.addClass('updating');

      $($qtyEl).parent().css({
        'pointer-events': 'none',
      });

      if ($(event.currentTarget).data('qty-decrease') !== undefined) {
        if (parseFloat(qtyValue) >= 2) {
          qtyValue = parseFloat(qtyValue) - 1;
        }
      } else if ($(event.currentTarget).data('qty-increase') !== undefined) {
        qtyValue = parseFloat(qtyValue) + 1;
      }

      axios.post('/cart/change.js', {
        quantity: qtyValue,
        line: itemIndex,
      })
      .then((response) => {
        app._getCartData();
        app.isLoading = false;
        $cartItem.removeClass('updating');
        $($qtyEl).parent().css({
          'pointer-events': 'inherit',
        });
      })
      .catch((error) => {
        // console.log(error)
      });
    },

    _initCartPromos() {
      const promoProductsObj = app.selectors.promoProductsObj;

      // stop parsing if we don't have the promo products json script tag when loading
      if (!$(promoProductsObj).html()) {
        return;
      }

      // set the promo products data
      const promoProducts = JSON.parse($(promoProductsObj).html());
      app.promoProducts = promoProducts.promoProducts;

      app.promoProducts.forEach((promo, index) => {
        app.promosTotal += parseFloat(promo.price);
        app._checkPromoQualifier(promo, index);
      });
    },

    _checkPromoQualifier(promo, index) {
      const cartItems = app.cart.items;
      const promoId = promo.id;
      const promoPrice = parseFloat(promo.price / 100);
      const qualifier = promo.qualifier;
      const mode = promo.mode;
      const teaserEnabled = promo.teaser;
      const qualifierAmount = parseFloat(promo.qualifierAmount);
      const discountCode = 'cart-promo-offers';

      // search cart items if GWP exists
      const isPromoInCart = cartItems.findIndex(item => item.product_id === promoId);

      if (isPromoInCart >= 0) {
        app.promoProducts[index].isInCart = true;

        // if promo is in cart we need to substract the promosTotal from cartTotal
        // we set a new cartTotal value
        app.cartTotal = (app.cart.total_price - app.promosTotal) / 100;

        // if promo exists in cart but it's the only one (orphan) remove it
        if (cartItems.length === 1) {
          app._removePromoFromCart(promoId);
        }

        // we need this to pass the discount code at the checkout URL
        // alter the cart form URL on the fly
        $(app.selectors.cartForm).attr('action', `/cart?discount=${discountCode}`);
      } else {
        app.cartTotal = app.cart.total_price / 100;
        app.promoProducts[index].isInCart = false;
      }

      if (qualifier === 'minimum_cart_amount') {
        app.promoProducts[index].isVisible = false;

        if (teaserEnabled) {
          app.promoProducts[index].isTeaserEnabled = true;
          app.promoProducts[index].isVisible = true;
        }

        if (app.cartTotal > qualifierAmount) { // qualifier rules are valid
          app._checkPromoUrlEnabler(promo, index);

          if (mode === 'auto') {
            app._addPromoToCart(promoId);
          }

          if (teaserEnabled) {
            app.promoProducts[index].isTeaserEnabled = false;
          }
        } else { // qualifier rules are invalid
          if (isPromoInCart >= 0) {
            // qualifier rules no longer valid => remove promo from cart
            app._removePromoFromCart(promoId);
          }

          if (teaserEnabled) {
            app.promoProducts[index].isTeaserEnabled = true;
            app._checkPromoUrlEnabler(promo, index);
          }
        }
      } else {
        app._checkPromoUrlEnabler(promo, index);

        if (mode === 'auto') {
          app._addPromoToCart(promoId);
        }
      }
    },

    _setGwpEnablerCodes() {
      const storedEnablerCodes = JSON.parse(sessionStorage.getItem('gwp_enabler_codes')) || [];
      const newUserCode = app._getUrlParameter('gwp_enabler_code');
      const storedUserCode = storedEnablerCodes.find(code => code === newUserCode);

      // push the user code from the url to the array in the session only if it doesn't exists
      if (newUserCode && !storedUserCode) {
        storedEnablerCodes.push(newUserCode);
        sessionStorage.setItem('gwp_enabler_codes', JSON.stringify(storedEnablerCodes));
      }

      app.gwpEnablerCodes = storedEnablerCodes;
    },

    _checkPromoUrlEnabler(promo, index) {
      const promUrlEnablerCode = promo.urlEnablerCode;
      const mathedPromoCode = app.gwpEnablerCodes.find(code => code === promUrlEnablerCode);

      // GWP enable with URL code (param) logic
      if (promUrlEnablerCode) {
        app.promoProducts[index].isVisible = false;

        if (mathedPromoCode) {
          app.promoProducts[index].isVisible = true;
        }
      }
    },

    _calcRemainingAmount(promoQualifierAmount) {
      promoQualifierAmount = parseFloat(promoQualifierAmount);
      const cartItems = app.cart.items;
      const cartTotal = parseFloat(app.cart.total_price / 100);
      let promosInCartTotal = 0;

      app.promoProducts.forEach((promo, index) => {
        const isPromoInCart = cartItems.findIndex(item => item.product_id === promo.id);

        if (isPromoInCart >= 0) {
          promosInCartTotal += parseFloat(promo.price);
        }
      });

      const remainingAmount = Math.abs((cartTotal - promoQualifierAmount - promosInCartTotal / 100) * 100);
      const remainingAmountMoney = app._formatMoney(remainingAmount);
      return remainingAmountMoney;
    },

    /**
     * Add promo product to cart
     *
     * @param {number} productId
     */
    _addPromoToCart(productId) {
      const cartItems = app.cart.items;
      const isPromoInCart = cartItems.findIndex(item => item.product_id === productId);

      // add promo to cart only if it's not already there
      if (isPromoInCart < 0) {
        clientShopify.get(`/admin/products/${productId}.json`, (err, data, headers) => {
          const product = data.product;

          // Before adding => create Price Rule
          app._createPriceRule(product);
        });
      }
    },

    /**
     * Remove promo product from cart
     *
     * @param {number} variantId
     */
    _removePromoFromCart(productId) {
      const cartItemIndex = app.cart.items.findIndex(item => item.product_id === productId) + 1;

      axios.post('/cart/change.js', {
        quantity: 0,
        line: cartItemIndex,
      })
      .then((response) => {
        location.reload();
      })
      .catch((error) => {
        // console.log(error)
      });
    },

    _createPriceRule(product) {
      const date = new Date();
      const newDate = date.toISOString();
      const discountCode = 'cart-promo-offers';
      const entitledProductIds = [];

      app.promoProducts.forEach((promo, index) => {
        entitledProductIds.push(promo.id);
      });

      // 1. Create Price Rule
      const priceRuleData = {
        price_rule: {
          title: discountCode,
          target_type: 'line_item',
          target_selection: 'entitled',
          allocation_method: 'across',
          entitled_product_ids: entitledProductIds, // A list of IDs of products that will be entitled to the discount. It can be used only with target_type set to line_item and target_selection set to entitled.
          once_per_customer: true,
          value_type: 'percentage',
          value: -100,
          customer_selection: 'all',
          starts_at: newDate,
        },
      };

      clientShopify.post('/admin/price_rules.json', priceRuleData, (err, data, headers) => {

        // 2. Create Discount Code and associate it to Price Rule
        const priceRuleId = data.price_rule.id;
        const discountData = {
          discount_code: {
            code: discountCode,
          },
        };
        clientShopify.post(`/admin/price_rules/${priceRuleId}/discount_codes.json`, discountData, (err, data, headers) => {

          // 3. if everything is succesful => add product to cart
          const mode = $('[data-promo-product]').data('mode');
          const variantId = product.variants[0].id;

          axios.post('/cart/add.js', {
            quantity: 1,
            id: variantId,
            properties: {
              promo_product: 'true',
              add_method: mode,
            },
          })
          .then((response) => {
            location.reload();
          })
          .catch((error) => {
            // console.log(error)
          });
        });
      });
    },

    /**
     * ShippingCalculator
     */
    _initShippingCalculator() {
      const $form = $('[data-shipping-calculator]');
      if ($form.length === 0) {
        return;
      }

      const customerCountry = $form.find('select[name="country"]').data('value').trim();
      const customerProvince = $form.find('select[name="province"]').data('value').trim();
      const customerZip = $form.find('input[name="zip"]').data('value').trim();

      // set date_ascending
      // if is user (is logged-in) show the user data
      // else get info from IP
      if (customerCountry) {
        app.selectedCountry = customerCountry;
        app.selectedProvince = customerProvince;
        app.selectedZip = customerZip;
      } else {
        axios.get('https://geoip-db.com/json/')
          .then((response) => {
            app.selectedCountry = response.data.country_name;
            app.selectedProvince = response.data.region_name;
            // app.selectedZip = response.data.zip_code
          })
          .catch((error) => {
            // console.log(error.response)
          });
      }
    },

    _getCartShippingRates() {
      app.isLoading = true;

      axios.post('/cart/prepare_shipping_rates', {
          shipping_address: {
            country: app.selectedCountry,
            province: app.selectedProvince,
            zip: app.selectedZip,
          },
        })
        .then((response) => {
          app._pollCartShippingRates();
        })
        .catch((error) => {
          // console.log(error.response.data)
          app.isLoading = false;
          app.shippingRatesErrors = error.response.data;
        });
    },

    _pollCartShippingRates() {
      axios.get('/cart/async_shipping_rates')
        .then((response) => {
          app.isLoading = false;
          if (response.data && response.data.shipping_rates) {
            app.shippingRates = response.data.shipping_rates;
          }
        })
        .catch((error) => {
          // console.log(error)
        });
    },

    /**
     * Vibrate mobile device
     */
    _vibrateDevice() {
      // check for vibration support
      const vibrationEnabled = navigator.vibrate || navigator.webkitVibrate || navigator.mozVibrate || navigator.msVibrate;
      if (vibrationEnabled) {
        navigator.vibrate(100);
      }
    },

    /* AddONs Methods */
    _readAddonsProduct(){
        const container = app.selectors.addOnsRead;
        const addonsVal = $(container).val();
        if ($(container).length > 0){
            const singleVal = addonsVal.split(',');
            const propValues = [];
            for (let i, e = 0, t = singleVal; e < t.length; e += 1) {
                axios.get(`/products/${t[e]}.js`)
                .then((response) => {
                    propValues[e] = response.data;
                })
                .catch((error) => {
                // console.log(error)
                });
            }
            app.addOns.product = propValues;
            app.addOns.item_count = singleVal.length;
        }

    },

    _selectedAddOnsProduct(event, variantId, price){
        let priceTotal;
        const priceEle = app.selectors.addOnsPrice;
        const productMain = $(priceEle).attr('data-priceVal');
        let selectVal = app.selectedAddOns.selectProduct;
        if (event.target.checked == true){
            priceTotal = parseFloat(price) + parseFloat(productMain);
            selectVal.push(variantId);
        } else if (event.target.checked == false){
            priceTotal = parseFloat(productMain) - parseFloat(price);
            selectVal = jQuery.grep(selectVal, (value) => {
                return value != variantId;
            });
        }
        $(priceEle).attr('data-priceVal', priceTotal);
        app.selectedAddOns.totalPrice = priceTotal;
        app.selectedAddOns.selectProduct = selectVal;
    },

    _getChangeVariantPrice(event, variantId, price){
        const priceEle = app.selectors.addOnsPrice;
        const selectedAddOnsVal = [];
        $.map($('.addons-slider-side'), (element) => {
            const $element = $(element).find('input');
            const type = $element.attr('type');
            const currentOption = {};
            if (type === 'radio' || type === 'checkbox') {
                if ($element[0].checked) {
                    currentOption.value = $element.val();
                    currentOption.price = $element.data('add-price');
                    selectedAddOnsVal.push(currentOption);
                    return currentOption;
                }
            }
        });
        const sum = selectedAddOnsVal.reduce((acc, item) => acc + item.price, 0);
        app.selectedAddOns.totalPrice = sum + price;
        $(priceEle).attr('data-priceval', parseFloat(sum + price));
    },

    _datePickerDelivary(){
        const holidays = app.holidays;
        const holidaysDisabled = [
            function(date) {
            // must return true to disable
            return (date.getDay() === 6 || date.getDay() === 0 || date.getDay() === 1);
            },
        ];
        for (let i, e = 0, t = holidays; e < t.length; e += 1) {
            const values = {};
            values.from = t[e],
            values.to = t[e];
            holidaysDisabled.push(values);
        }

        flatpickr('#deliveryDateConfig', {
            altFormat: 'M j, Y',
            altInput: true,
            disableMobile: 'true',
            mode: 'single',
            disableMobile: 'true',
            dateFormat: 'm-d-Y',
            enableTime: false,
            onClose(selectedDates, dateStr, instance){
              $('.flatpickr-input').blur();
            },
            locale: {
                firstDayOfWeek: 1, // start week on Monday
            },
            // minDate: "today",
            minDate: new Date().fp_incr(7), // 2 days from now

            // disable Saturdays and Sundays and Mondays
            // disable using a function
            disable: holidaysDisabled,
        });
    },

    _giftCartForm(event){
        app.isGiftMsg = false;
        if (event.target.checked == true){
            app.isGiftcartForm = false;            
        } else if (event.target.checked == false){
            app.isGiftcartForm = true;
            $('#GiftAutoRenew').prop('checked', false);
        }
    },

    _giftCartAutoRenew(event){
        if (event.target.checked == true){
            $(event.target).val('renewal');
        } else if (event.target.checked == false){
            $(event.target).val('once');
        }
    },

    _btnRedirectDrwaer(){
        jQuery.getJSON('/cart.js', (cart) => {
          // now have access to Shopify cart object
          $.each(cart.items, (index, value) => {
              if (value.properties != null){
				  if (value.properties.shipping_interval_unit_type != null){
                  	if (value.properties.shipping_interval_unit_type != undefined){
                          app.isCheckOutURl = true;
                      }
                  }
              }
          });
          if (app.isCheckOutURl == true){
              app._redirectToCheckout();
          } else {
            window.location.href = '/checkout';
          }
      });
    },

    _redirectToCheckout(){
        function reChargeGetCookie(name) {
            return (document.cookie.match(`(^|; )${name}=([^;]*)`) || 0)[2];
          }

          // Build the Checkout URL
          let ga_linker = '';
          const myshopify_domain = app.domain;
          const token = reChargeGetCookie('cart');
          try {
            // Cross-domain tracking with Google Analytics
            ga_linker = ga.getAll()[0].get('linkerParam');
          } catch (e) {
            // 'ga' is not available
            ga_linker = '';
          }
          // Customer email address for use in the Checkout URL
          const customer_param = `customer_id=${app.customer.id}&customer_email=${app.customer.email}{% endif %}`;
          const checkout_url = `https://checkout.rechargeapps.com/r/checkout?myshopify_domain=${myshopify_domain}&cart_token=${token}&${ga_linker}&${customer_param}`;
          window.location.href = checkout_url;
          localStorage.setItem('gift_enabled', false);
    },
  },
};

export default cartMethods;
