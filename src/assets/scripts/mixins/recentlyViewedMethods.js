/**
 * Recently Viewed Products methods
 * -----------------------------------------------------------------------------
 *
 * @namespace recentlyViewedMethods
 */

import axios from 'axios';
import Flickity from 'flickity';


const recentlyViewedMethods = {
  methods: {
    _initRecentlyViewed() {
      const $container = $(app.selectors.productContainer);
      let productObj = app.selectors.productObj;
      let currentProductHandle;
      let currentVariantId;

      app._getRecentlyViewed();

      // product page
      if ($(productObj, $container).html()) {
        productObj = JSON.parse($(productObj, $container).html());
        currentProductHandle = productObj.handle;
        currentVariantId = app.currentVariant.id;

        app._addVariantToRecentlyViewed(currentProductHandle, currentVariantId);

        // filter data
        // exclude the current visited product
        app.recentlyViewed = app.recentlyViewed.filter(item => item.id !== currentVariantId);
      }
    },

    /**
     * Gets the recentlyViewed Array from localStorage
     */
    _getRecentlyViewed() {
      let recentlyViewed = localStorage.getItem('user_recentlyViewed');
      if (!recentlyViewed){
        return;
      }
      recentlyViewed = JSON.parse(recentlyViewed);
      // reverse
      recentlyViewed.reverse();
      // set data
      app.recentlyViewed = recentlyViewed;
    },

    /**
     * Checks if a variant exists in the recentlyViewed Array
     * @param  {variant}
     * @return {boolean}
     */
    _checkRecentlyViewed(varianId) {
      const index = this.recentlyViewed.findIndex(x => x.id === varianId);

      if (index === -1) {
        return false;
      }
      return true;
    },

    /**
     * Adds the selected variant in recentlyViewed Array
     * @param  {productHandle varianId}
     *
     */
    _addVariantToRecentlyViewed(productHandle, varianId) {
      const index = app.recentlyViewed.findIndex(x => x.id === varianId);

      // push object to recentlyViewed Array only if it doesn't exists
      if (index === -1) {
        const currentVariant = app.currentVariant;

        // add additional properties to the object
        currentVariant.product_id = app.product.id;
        currentVariant.product_handle = productHandle;

        // default image
        if (currentVariant.option1 === 'Default Title' || app.currentVariant.featured_image === null) {
          currentVariant.featured_image = app.product.featured_image;
        } else {
          currentVariant.featured_image = app.currentVariant.featured_image.src;
        }

        app.recentlyViewed.push(currentVariant);
        localStorage.setItem('user_recentlyViewed', JSON.stringify(app.recentlyViewed));
      }
    },

    /**
     * Adds a variant from recentlyViewed Array
     * @param  {variantId}
     *
     */
    _removeFromToRecentlyViewed(varianId) {
      const index = app.recentlyViewed.findIndex(x => x.id === varianId);

      if (index > -1) {
        app.recentlyViewed.splice(index, 1);
        localStorage.setItem('user_recentlyViewed', JSON.stringify(app.recentlyViewed));
      }
    },

    /**
     * Returns the variant Url from a given variant Id
     * @param  {productHandle, variantId}
     * @return {string}
     *
     */
    _getvariantUrl(productHandle, variantId) {
      return `/products/${productHandle}?variant=${variantId}`;
    },

  },
};

export default recentlyViewedMethods;
