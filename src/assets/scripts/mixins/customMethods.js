/**
 * Custom methods
 * -----------------------------------------------------------------------------
 *
 * @namespace customMethods
 */

// media queries in Javascript => http://wicky.nillia.ms/enquire.js/
import enquire from 'enquire.js';
// detect if a device is mouseOnly, touchOnly, or hybrid => http://detect-it.rafrex.com/
import detectIt from 'detect-it';
import '../vendor/meanmenu';


const customMethods = {

  methods: {
    _initCustomMethods() {
      // custom JS code
      app._header();      
      
      var fontFace= '';
      var styleTag;
      if(app.currentTemplate != null && app.currentTemplate == 'index'){       
        $('[data-header-carousel] [data-carousel-cell]').each(function(){           
            if($(this).find('[data-font-face]:hidden').length > 0){                
                if(fontFace == '')
                    fontFace = $(this).find('[data-font-face]:hidden').html();                
                else
                    fontFace = fontFace + $(this).find('[data-font-face]:hidden').html();
            }            
        });

        styleTag = '<style type="text/css">'
                   + fontFace
                   +'</style>';
        $('head').append(styleTag);
      }

      app._sliderOfWorksPlan();
    },
    _header(){
      $('#mean-menu').meanmenu();
      $('.back').on('click', function(){
       $(this).parent('ul').removeClass('slide_active');
       $(this).parent('ul').siblings('a').removeClass('mean-clicked');
     });

       $('.search-icon-toggle').click(() => {
           $('#mobile-search').slideToggle();
           $('.overlay').toggleClass('show');
           $('body').css('overflow', 'auto');
       });

       $('.filtersCollapse').click((e) => {
           e.preventDefault();
            $('#filtersCollapse').addClass('show');
            $('.overlay').addClass('show');
            $('html').addClass('popup-opend');
        });

        $('.close-filter').click(() =>{
            $('#filtersCollapse').removeClass('show');
            $('.overlay').removeClass('show');
            $('html').removeClass('popup-opend');
        })

        $('html').click(function() {
            $('#filtersCollapse').removeClass('show');
            $('.overlay').removeClass('show');
            $('html').removeClass('popup-opend');
        });

        $('#filtersCollapse, .filtersCollapse').click((event) => {
            event.stopPropagation();
        });

        // $('a[href*="#"]').on('click', function (e) {
        //     e.preventDefault();

        //     $('html, body').animate({
        //         scrollTop: $($(this).attr('href')).offset().top
        //     }, 500, 'linear');
        // });

   },

   _sliderOfWorksPlan(){

	    $('.slick-carousel').slick({
                responsive: [
                    {
                        breakpoint: 992,
                        settings: {
                        slidesToShow: 3,
                        },
                    },
                    {
                        breakpoint: 768,
                        settings: {
                        slidesToShow: 2,
                        },
                    },
                    {
                        breakpoint: 480,
                        settings: {
                        slidesToShow: 1,
                        },
                    },
                ],
            });

            $('[data-addons-sliderInPopup]').slick({

                responsive: [
                    {
                        breakpoint: 1201,
                        settings: {
                        slidesToShow: 3,
                        },
                    },
                    {
                        breakpoint: 768,
                        settings: {
                        slidesToShow: 2,
                        },
                    },
                    {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 1,
                        },
                    },
                ],
            });


   },
    _closeAnnousement(event) {
      $(event.currentTarget).closest('.hello--bar').hide();
      app._initStickyHeader();
    },

    _register(event) {
        const $form = $(event.target).closest('form');
        $form.find('#mc-embedded-subscribe').val('Sending...');
        $.ajax({
            type: $form.attr('method'),
            url: $form.attr('action'),
            data: $form.serialize(),
            cache: false,
            dataType: 'json',
            crossDomain: true,
            contentType: 'application/json; charset=utf-8',
            error(err) { 
                $form.find('#mc-embedded-subscribe').val('Sign Me Up');
                $form.find('#subscribe-result').html('Could not connect to the registration server. Please try again later.'); 
            },
            success(data) {
                $form.find('#mc-embedded-subscribe').val('Sign Me Up');
                if (data.result === 'success') {
                    // Yeahhhh Success
                    $form.find('#subscribe-result').css('color', 'rgb(53, 114, 210)');
                    $form.find('#subscribe-result').html('<p>Thank you for subscribing. We have sent you a confirmation email.</p>');
                    $form.find('#mce-EMAIL').val('');
                } else {
                    // Something went wrong, do something to notify the user.
                    $form.find('#mce-EMAIL').css('borderColor', '#ff8282');
                    $form.find('#subscribe-result').css('color', '#ff8282');
                    $form.find('#subscribe-result').html(`<p>${data.msg.substring(4)}</p>`);
                }
            },
        });
    },

  },
};

export default customMethods;
