/* eslint-disable promise/always-return */
  /**
 * Recharge methods
 * -----------------------------------------------------------------------------
 *
 * @namespace rechargeMethods
 */

import axios from 'axios';
import moment from 'moment';
import flatPickr from 'vue-flatpickr-component';
import 'flatpickr/dist/flatpickr.css';


const rechargeAPI = axios.create({
    baseURL: 'https://work.planetx.in/recharge/demo-2016/api/common',
    headers: {
      Accept: 'application/json, text/javascript, /; q=0.01',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Headers': '*',
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
      'Cache-Control': 'must-revalidate',
      'Cache-Control': 'no-cache',
      'Cache-Control': 'no-store',
      Pragma: 'no-cache',
      crossDomain: true,
    },
});
const rechargeSUbAPI = axios.create({
    baseURL: 'https://work.planetx.in/recharge/demo-2016/api/subscription',
    headers: {
      Accept: 'application/json, text/javascript, /; q=0.01',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Headers': '*',
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
      'Cache-Control': 'must-revalidate',
      'Cache-Control': 'no-cache',
      'Cache-Control': 'no-store',
      Pragma: 'no-cache',
      crossDomain: true,
    },
});

const rechargeMethods = {

    components: {
        flatPickr,
    },

    data() {
        return {

        };
    },

  methods: {

    _initRecharge() {
        if (app.currentTemplate !== 'account') {
            return;
        }
        app._getSubscriptions();
    },

    _footableJs(){
        $('.footable').footable({
            breakpoints: {
                phone: 581,
                halfTab: 767,
                tablet: 1024,
            },
        });
    },

    /**
     * Gets the wishlist Array from Local localStorage
     */
     _getSubscriptions(){
      const config = {
          data: {
              shopify_customer_id: app.customersID,
              limit: 250,
          },
      };
      
      rechargeSUbAPI.post(app.rechargeDashbord.getSubscriptionAPI, config)
        .then((response) => {
            console.log(response);
            if (response.data.error) {
                return false;
            } else {
                app.isgetsubscription = true;
                app.rechargeDashbord.subscription = response.data.data.subscriptions;
            }
        }).then((response) => {
            app._footableJs();
        }).catch((error) => {
            console.log(error);
        });
    },

    /**
     * Get Product Json
     * @param  {productID}
     * @return {boolean}
     */

    _getImgSrc(productID, variantID){
        let src = '';
        const config = {
            data: {
                products: productID,
            },
        };
        rechargeAPI.post(window.theme.rechargeProductAPI, config)
        .then((response) => {
            // console.log(response);
            if (response.data.error) {
                return false;
            } else {
                src = response.data.data.products[0].image.src;
                $(`.img_${variantID}`).attr('src', app._getSizedImageUrl(src, 'small'));
            }
        }).catch((error) => {
            console.log(error);
        });
        // return src;
    },

     _readGetProduct(productID){
        const config = {
            data: {
                products: productID,
            },
        };
        rechargeAPI.post(window.theme.rechargeProductAPI, config)
        .then((response) => {
            if (response.data.error) {
                return false;
            } else {
                if (app.rechargeDashbord.currentSubscription.order_interval_unit == 'month') {
                    app.rechargeDashbord.currentProduct.monthly = response.data.data.products[0];
                    app.rechargeDashbord.currentProduct.monthly.selected = true;
                    $.each(app.rechargeDashbord.currentProduct.monthly.variants, (index, value) => {
                        // console.log(value);
                        if (value.id == app.rechargeDashbord.currentSubscription.shopify_variant_id) {
                            app.rechargeDashbord.currentVariant.monthly = value;
                            app.rechargeDashbord.currentVariant.monthly.selected = true;
                        }
                    });
                } else {
                    app.rechargeDashbord.currentProduct.weekly = response.data.data.products[0];
                    app.rechargeDashbord.currentProduct.weekly.selected = true;
                    $.each(app.rechargeDashbord.currentProduct.weekly.variants, (index, value) => {
                        if (value.id == app.rechargeDashbord.currentSubscription.shopify_variant_id) {
                            app.rechargeDashbord.currentVariant.weekly = value;
                            app.rechargeDashbord.currentVariant.weekly.selected = true;

                        }
                    });
                }
                app.rechargeDashbord.selectProduct.productSelected = response.data.data.products[0];
                app._addedCustomAttrInProduct(app.rechargeDashbord.selectProduct.productSelected, app.rechargeDashbord.currentSubscription.price);
            }
        }).catch((error) => {
            console.log(error);
        });
     },

     _getRechargeProduct(productID, index){
        const config = {
            data: {
                products: productID,
                withInfo: 'metafields',
            },
        };
        rechargeAPI.post(window.theme.rechargeProductAPI, config)
        .then((response) => {
            if (response.data.error) {
                return false;
            } else {
                app.rechargeDashbord.subscription[index].shopifyProductJson = response.data.data.products[0];
            }
        }).catch((error) => {
            console.log(error);
        });
     },


    /**
     * Get Product Json
     * @param  {address ID}
     * @return {boolean}
     */
     _getAddress(address_id){
        //  console.log(address_id);
        const config = {
            data: {
                url: `/addresses/${address_id}`,
                method: 'GET',
            },
          };
          rechargeAPI.post(app.rechargeDashbord.API, config)
            .then((response) => {
                // console.log(response);
                if (response.data.error) {
                    return false;
                } else {
                    app.rechargeDashbord.address = response.data.data.address;
                    // eslint-disable-next-line eqeqeq
                    if (app.rechargeDashbord.address.value != undefined) {
                        app.rechargeDashbord.gift_message.to = app.rechargeDashbord.address.cart_attributes[0].value;
                        app.rechargeDashbord.gift_message.from = app.rechargeDashbord.address.cart_attributes[2].value;
                        app.rechargeDashbord.gift_message.message = app.rechargeDashbord.address.cart_attributes[1].value;
                    }
                }
            }).catch((error) => {
                console.log(error);
            });
    },

     /**
     * Get Recharge Dashboard
     * @param  {Event}
     * @return {boolean}
     */

    _toggleRechargeDrawer(event, index){
        app.rechargeDashbord.currentSubscription = app.rechargeDashbord.subscription[index];
        const $siteOverlay = $(app.selectors.siteOverlay);
        if (!app.isRechargeDrawerOpen) { // open drawer
            app.isOverlayVisible = true;
            app._lockScroll();
        } else {
            app.isOverlayVisible = false;
            app._unlockScroll();
        }

        app.isRechargeDrawerOpen = !app.isRechargeDrawerOpen;
        $(document).on('keydown', (e) => { // close drawer
            if (!app.isRechargeDrawerOpen){
              return;
            }
            e = e || window.event;
            if (e.keyCode === 27){
                app._toggleRechargeDrawer();
            }
        });

    },

    /**
     * Get Recharge Dashboard
     * @param  {Popup Target}
     * @return {boolean}
     */

    _getCurrentTarget(event, popupTarget){
        switch (popupTarget) {
            case 'change-box':
                app.isChangeSubscriptionBox = true;
                app._updateSubscriptionProduct();
                if (app.rechargeDashbord.currentSubscription.order_interval_unit == 'month'){
                    app.rechargeDashbord.currentProduct.weekly = app.rechargeDashbord.products.weekly[0];
                } else {
                    app.rechargeDashbord.currentProduct.monthly = app.rechargeDashbord.products.monthly[0];
                }
                setTimeout(() => {
                    $(`[data-${app.rechargeDashbord.currentSubscription.order_interval_unit}-ele] [data-product] option[value="${app.rechargeDashbord.currentSubscription.shopify_product_id}"]`).attr('selected', 'selected');

                    $(`[data-${app.rechargeDashbord.currentSubscription.order_interval_unit}-ele] [data-variant] option[value="${app.rechargeDashbord.currentSubscription.shopify_variant_id}"]`).attr('selected', 'selected');
                }, 500);

                app.rechargeDashbord.selectProduct.selectedPlan = app.rechargeDashbord.currentSubscription.order_interval_unit;
                // console.log(app.rechargeDashbord.selectProduct.selectedPlan);
                break;
            case 'skip-shipment-box':
                app.isSkipBox = true;
                setTimeout(() => {
                    app._pauseSubscriptionPicker();
                }, 500);
                break;
            case 'edit-shipping-box':
                app.isAddressesBox = true;
                setTimeout(() => {
                    new Shopify.CountryProvinceSelector('AddressCountryNew', 'AddressProvinceNew', {
                        hideElement: 'AddressProvinceContainerNew',
                    });
                    $(`#AddressProvinceNew option[value="${app.rechargeDashbord.address.province}"]`).attr('selected', 'selected');
                    $(`#AddressCountryNew option[value="${app.rechargeDashbord.address.country}"]`).attr('selected', 'selected');
                }, 300);
                app._getChangeShippingAddress();
                break;
            case 'gift-message-box':
                app._getChangeShippingAddress();
                app.isGiftNote = true;
                break;
            case 'cancel-subscription-box':
                app.isCancel = true;
                break;
        }
    },

    _gobackPopup(event){
        app.isCancel = false;
        app.isGiftNote = false;
        app.isAddressesBox = false;
        app.isSkipBox = false;
        app.isSkipUpdate = false;
        app.isChangeSubscriptionBox = false;
        app.isUpdateSubscription = false;
        // flatpickr('#pauseSubscriptionPicker').onClose();
    },

    /**
     * Get Update Subscription
     * @param  {subscription ID}
     * @return {boolean}
     */

     _updateSubscriptionProduct(){
        //  console.log('======');
         $('[data-product-subscribe-json]').each((index, value) => {
            console.log(value);
            const idSubscription = $(value).attr('id');
            if (idSubscription.indexOf('monthly') > -1){
                const prodDetails = $(value).html();
                const planProductJson = JSON.parse(prodDetails);
                app.rechargeDashbord.products.monthly = planProductJson;
                $.each(app.rechargeDashbord.products.monthly, (index, value) => {
                    app._addedCustomAttrInProduct(value, value.price);
                });
            } else {
                const prodDetails = $(value).html();
                const planProductJson = JSON.parse(prodDetails);
                app.rechargeDashbord.products.weekly = planProductJson;
                $.each(app.rechargeDashbord.products.weekly, (index, value) => {
                    app._addedCustomAttrInProduct(value, value.price);
                });
            }
        });
     },

     _selectedVariant(plan, index){
        if (plan == 'month') {
            app.rechargeDashbord.currentVariant.monthly = app.rechargeDashbord.currentProduct.monthly.variants[index];
            app.rechargeDashbord.selectProduct.variantSelected = app.rechargeDashbord.currentVariant.monthly;
        } else {
            app.rechargeDashbord.currentVariant.weekly = app.rechargeDashbord.currentProduct.weekly.variants[index];
            app.rechargeDashbord.selectProduct.variantSelected = app.rechargeDashbord.currentVariant.weekly;
        }
     },

     _firstAvailableVar(plan){
        if (plan == 'month') {
            app.rechargeDashbord.currentVariant.monthly = app.rechargeDashbord.currentProduct.monthly.variants[0];
            app.rechargeDashbord.selectProduct.variantSelected = app.rechargeDashbord.currentVariant.monthly;
        } else {
            app.rechargeDashbord.currentVariant.weekly = app.rechargeDashbord.currentProduct.weekly.variants[0];
            app.rechargeDashbord.selectProduct.variantSelected = app.rechargeDashbord.currentVariant.weekly;
        }
     },

     _changePlan(event, plan){
        $('.mission-selector').removeClass('active');
        $(event.currentTarget).closest('.mission-selector').addClass('active');
        app.rechargeDashbord.selectProduct.selectedPlan = plan;
        const index = $(event.currentTarget).closest('.mission-selector').find('[data-product] option:selected').attr('data-index');
        // console.log('index===>',index);
        let varindex;
        if (plan == 'month') {
            app.rechargeDashbord.currentProduct.monthly = app.rechargeDashbord.products.monthly[index];
            app.rechargeDashbord.selectProduct.productSelected = app.rechargeDashbord.products.monthly[index];
            if (app.rechargeDashbord.currentProduct.monthly.variants.length > 0) {
                varindex = $(event.currentTarget).closest('.mission-selector').find('[data-variant] option:selected').attr('data-var-index');
                app._selectedVariant('month', varindex);
            } else {
                app._firstAvailableVar('month');
            }
        } else {
            app.rechargeDashbord.currentProduct.weekly = app.rechargeDashbord.products.weekly[index];
            app.rechargeDashbord.selectProduct.productSelected = app.rechargeDashbord.products.weekly[index];
            if (app.rechargeDashbord.currentProduct.weekly.variants.length > 0) {
                varindex = $(event.currentTarget).closest('.mission-selector').find('[data-variant] option:selected').attr('data-var-index');
                app._selectedVariant('week', varindex);
            } else {
                app._firstAvailableVar('week');
            }
        }
        if (app.rechargeDashbord.currentSubscription.shopify_variant_id == app.rechargeDashbord.selectProduct.variantSelected.id){
            app.isUpdateSubscription = false;
        } else {
            app.isUpdateSubscription = true;
        }
     },


     _chooseSubscriptionPlan(event, plan){
        const productHandle = $(event.currentTarget).val();
        const index = $(event.currentTarget).find('option:selected').attr('data-index');
        let varindex;
        if (plan == 'monthly'){
            app.rechargeDashbord.currentProduct.monthly = app.rechargeDashbord.products.monthly[index];
            app.rechargeDashbord.selectProduct.productSelected = app.rechargeDashbord.products.monthly[index];
            if (app.rechargeDashbord.currentProduct.monthly.variants.length > 0) {
                varindex = $(event.currentTarget).closest('.mission-selector').find('[data-variant] option:selected').attr('data-var-index');
                app._selectedVariant('month', varindex);
            } else {
                app._firstAvailableVar('month');
            }
        } else {
            app.rechargeDashbord.currentProduct.weekly = app.rechargeDashbord.products.weekly[index];
            app.rechargeDashbord.selectProduct.productSelected = app.rechargeDashbord.products.weekly[index];
            if (app.rechargeDashbord.currentProduct.weekly.variants.length > 0) {
                varindex = $(event.currentTarget).closest('.mission-selector').find('[data-variant] option:selected').attr('data-var-index');
                app._selectedVariant('week', varindex);
            } else {
                app._firstAvailableVar('week');
            }
        }
        if (app.rechargeDashbord.currentSubscription.shopify_variant_id == app.rechargeDashbord.selectProduct.variantSelected.id){
            app.isUpdateSubscription = false;
        } else {
            app.isUpdateSubscription = true;
        }
     },


     _chooseSubscriptionVariantPlan(event){
        const variantID = $(event.currentTarget).val();
        const index = $(event.currentTarget).find('option:selected').attr('data-var-index');
        if (app.rechargeDashbord.selectProduct.selectedPlan == 'month') {
            if (app.rechargeDashbord.currentProduct.monthly.variants.length > 0) {
                app._selectedVariant('month', index);
            } else {
                app._firstAvailableVar('month');
            }
        } else if (app.rechargeDashbord.currentProduct.weekly.variants.length > 0) {
                app._selectedVariant('week', index);
            } else {
                app._firstAvailableVar('week');
            }
        app.isUpdateSubscription = true;
     },


     _updateAPICall(subs_id){
        let config = '';
        let cancelConfig = '';
        const price = app._formatMoney(app.rechargeDashbord.selectProduct.variantSelected.price);
        if (parseInt(app.rechargeDashbord.currentSubscription.charge_interval_frequency) != parseInt(app.rechargeDashbord.selectProduct.variantSelected.charge_interval_frequency)) {
            
            config = {
                data: {
                    url: '/subscriptions/',
                    method: 'POST',
                    data: {
                        address_id: app.rechargeDashbord.currentSubscription.address_id,
                        next_charge_scheduled_at: app.rechargeDashbord.currentSubscription.next_charge_scheduled_at,
                        product_title: app.rechargeDashbord.selectProduct.productSelected.title,
                        price: price.replace('$', ''),
                        quantity: 1,
                        sku: app.rechargeDashbord.currentVariant.sku,
                        shopify_variant_id: app.rechargeDashbord.selectProduct.variantSelected.id,
                        order_interval_unit: app.rechargeDashbord.selectProduct.productSelected.shipping_interval_unit_type,
                        order_interval_frequency: app.rechargeDashbord.selectProduct.productSelected.shipping_interval_frequency,
                        charge_interval_frequency: app.rechargeDashbord.selectProduct.variantSelected.charge_interval_frequency,
                    },
                },
            };

            cancelConfig = {
                data: {
                    url: `/subscriptions/${subs_id}/cancel`,
                    method: 'POST',
                    data: {
                        cancellation_reason: `Subscription upgraded to ${app.rechargeDashbord.selectProduct.variantSelected.title}`,
                    },
                },
            };

        } else {
            config = {
                data: {
                    url: `/subscriptions/${subs_id}`,
                    method: 'PUT',
                    data: {
                        product_title: app.rechargeDashbord.selectProduct.productSelected.title,
                        variant_title: app.rechargeDashbord.selectProduct.variantSelected.title,
                        price: price.replace('$', ''),
                        shopify_variant_id: app.rechargeDashbord.selectProduct.variantSelected.id,
                        order_interval_unit: app.rechargeDashbord.selectProduct.productSelected.shipping_interval_unit_type,
                        order_interval_frequency: app.rechargeDashbord.selectProduct.productSelected.shipping_interval_frequency,
                        charge_interval_frequency: app.rechargeDashbord.selectProduct.variantSelected.charge_interval_frequency,
                        sku: app.rechargeDashbord.currentVariant.sku,
                        shopify_product_id: app.rechargeDashbord.selectProduct.productSelected.id,
                        quantity: 1,
                    },
                },
            };
        }
        
        app.isLoading = true;
        app.loadingEvent = 'updateSubscription';
        rechargeAPI.post(app.rechargeDashbord.API, config)
        .then((response) => {
            if (response.data.data.error) {
                app.isLoading = false;
                app.loadingEvent = null;
                app._snotifyMsg('Error', response.data.data.error, 'Error');
                return false;
            } else {
                setTimeout(() => {
                });
                if (parseInt(app.rechargeDashbord.currentSubscription.charge_interval_frequency) != parseInt(app.rechargeDashbord.selectProduct.variantSelected.charge_interval_frequency)) {
                    rechargeAPI.post(app.rechargeDashbord.API, cancelConfig)
                        .then((response) => {                        
                            if (response.data.data.error) {
                                app.isLoading = false;
                                app.loadingEvent = null;
                                app._snotifyMsg('Error', response.data.data.error, 'Error');
                                return false;
                            } else {
                                app._getSubscriptions();
                                app.isRechargeDrawerOpen = false;
                                app.isOverlayVisible = false;
                                app._unlockScroll();
                                app.isChangeSubscriptionBox = false;
                                app.isUpdateSubscription = false;
                                app._snotifyMsg('Success', 'Your Subscription has been Updated Successfully', 'Success');
                                setTimeout(() => {
                                    app.isLoading = false;
                                    app.loadingEvent = null;
                                }, 500);
                            }
                        }).catch((error) => {
                            console.log(error);
                        });
                } else {
                    app._getSubscriptions();
                    app.isRechargeDrawerOpen = false;
                    app.isOverlayVisible = false;
                    app._unlockScroll();
                    app.isChangeSubscriptionBox = false;
                    app.isUpdateSubscription = false;
                    app._snotifyMsg('Success', 'Your Subscription has been Updated Successfully', 'Success');
                    setTimeout(() => {
                        app.isLoading = false;
                        app.loadingEvent = null;
                    }, 500);
                }


            }
        }).catch((error) => {
            console.log(error);
        });
     },
    /**
     * Get Skip Subscription
     * @param  {subscription ID}
     * @return {boolean}
     */
    _get_nextdate(id, nextChargeScheduled, orderUnit, orderFrequency){
        let nextChargeDate = '';
        let SimplenextChargeDate = '';
        let orderWeek = '';
        switch (orderUnit) {
            case 'month':
                nextChargeDate = moment(nextChargeScheduled).add(1, 'month').format('MMMM D, YYYY');
                SimplenextChargeDate = moment(nextChargeScheduled).add(1, 'month').format('Y-M-DD');
                app.rechargeDashbord.skip_shippment.dateFormate = nextChargeDate;
                app.rechargeDashbord.skip_shippment.nextChargeDate = SimplenextChargeDate;
                break;
            case 'week':
                orderWeek = parseInt(orderFrequency) * 7;
                nextChargeDate = moment(nextChargeScheduled).add(orderWeek, 'days').format('MMMM D, YYYY');
                SimplenextChargeDate = moment(nextChargeScheduled).add(orderWeek, 'days').format('Y-M-DD');
                app.rechargeDashbord.skip_shippment.dateFormate = nextChargeDate;
                app.rechargeDashbord.skip_shippment.nextChargeDate = SimplenextChargeDate;
                break;
            case 'day':
                nextChargeDate = moment(nextChargeScheduled).add(orderFrequency, 'day').format('MMMM D, YYYY');
                SimplenextChargeDate = moment(nextChargeScheduled).add(orderFrequency, 'day').format('Y-M-DD');
                app.rechargeDashbord.skip_shippment.dateFormate = nextChargeDate;
                app.rechargeDashbord.skip_shippment.nextChargeDate = SimplenextChargeDate;
                break;
        }
    },

    _selectButtonHide(event){
        const $container = $(app.selectors.rechargePopup);
        const singleOptionSelector = app.selectors.skipRecharge;
        $.map($(singleOptionSelector, $container), (element) => {
            const $element = $(element);
            const type = $element.attr('type');
            const currentPick = {};
            if (type === 'radio' || type === 'checkbox') {
                if ($element[0].checked) {
                    app.isSkipUpdate = true;
                    app.rechargeDashbord.skip_shippment.textSkip = $element.val();
                    return currentPick;
                }
                return false;
            }
            app.isSkipUpdate = true;
            app.rechargeDashbord.skip_shippment.textSkip = $element.val();
            return currentPick;
        });
    },

    _updateNextChargeDate(id){        
        let nextChargeDate = '';
        if (app.rechargeDashbord.skip_shippment.textSkip == 'Skip') {
            nextChargeDate = app.rechargeDashbord.skip_shippment.nextChargeDate;
            nextChargeDate = `${nextChargeDate}T00:00:00`;
        } else if (app.rechargeDashbord.skip_shippment.textSkip == 'Pause'){
            if ($('#pauseSubscriptionPicker').val() == ''){
                $('.flatpickr-input').addClass('border-danger');
                return false;
            }
            nextChargeDate = `${$('#pauseSubscriptionPicker').val()}T00:00:00`;
        }

        
        const config = {
            data: {
                url: `/subscriptions/${id}/set_next_charge_date`,
                method: 'POST',
                data: {
                    date: nextChargeDate,
                },
            },
        };
        app.isLoading = true;
        app.loadingEvent = 'skipSubscription';
        rechargeAPI.post(app.rechargeDashbord.API, config)
        .then((response) => {
            console.log(response);
            if (response.data.data.error) {
                app._snotifyMsg('Error', response.data.data.error, 'Error');
                return false;
            } else {
                app._getSubscriptions();
                app.isRechargeDrawerOpen = false;
                app.isOverlayVisible = false;
                app._unlockScroll();
                app.isSkipUpdate = false;
                app.isSkipBox = false;
                app._snotifyMsg('Shipment', `Your subscription ${app.rechargeDashbord.skip_shippment.textSkip} successfully `, 'Success');
                setTimeout(() => {
                    app.isLoading = false;
                    app.loadingEvent = null;
                }, 500);
            }
        }).catch((error) => {
            console.log(error);
        });
    },

    _formatDateRecharge(value) {
        const date = moment(value).format('MMM D, YYYY');
        return date;
    },

    /**
     * Get Shipping Address
     * @param  {}
     * @return {boolean}
     */
    _getChangeShippingAddress(){
        $('body').keyup('.edit-shipping-box input,.edit-shipping-box textarea', () => {
            app.isupdateAddress = true;
            app.isGiftUpdate = true;
        });
        $('body').change('.edit-shipping-box select', () => {
            app.isupdateAddress = true;
        });
    },

    _updateAddress(address_id){
        const address_first_name = $('[data-first-name]').val();
        const address_last_name = $('[data-last-name]').val();
        const address_address1 = $('[data-address1]').val();
        const address_address2 = $('[data-address2]').val();
        const address_city = $('[data-city]').val();
        const address_province = $('[data-province]').val();
        const address_country = $('[data-country]').val();
        const address_zip = $('[data-zip]').val();
        const address_company = $('[data-company]').val();
        const config = {
            data: {
                url: `/addresses/${address_id}`,
                method: 'PUT',
                data: {
                    first_name: address_first_name,
                    last_name: address_last_name,
                    address1: address_address1,
                    address2: address_address2,
                    city: address_city,
                    province: address_province,
                    country: address_country,
                    zip: address_zip,
                    company: address_company,
                },
            },
        };

        app.isLoading = true;
        app.loadingEvent = 'updateSubscription';
        rechargeAPI.post(app.rechargeDashbord.API, config)
        .then((response) => {
            if (response.data.data.error) {
                app.isLoading = false;
                app.loadingEvent = null;
                if (response.data.data.error.zip){
                    app._snotifyMsg('Wrong Zipcode', response.data.data.error.zip, 'Error');
                } else {
                    app._snotifyMsg('Error', response.data.data.error, 'Error');
                }
                return false;
            } else {
                app._getSubscriptions();
                app._getAddress(address_id);
                app.isRechargeDrawerOpen = false;
                app.isOverlayVisible = false;
                app._unlockScroll();
                app.isAddressesBox = false;
                app.isupdateAddress = false;
                app._snotifyMsg('Success', 'Your Address has been Updated Successfully', 'Success');
                setTimeout(() => {
                    app.isLoading = false;
                    app.loadingEvent = null;
                }, 500);
            }
        }).catch((error) => {
            console.log(error);
        });
    },

    /*************Eneded Script Shipping Address Subscription *****************/

    /**
     * Get Cancel Subscription
     * @param  {}
     * @return {boolean}
     */
    _getReasonFromOptions(event){
        const currentOption = $(event.currentTarget).val();
        if (currentOption == 'no_reson') {
            app.isCancelOption = false;
        } else {
            app.isCancelOption = true;
        }
    },

    _putCancelSubscription(event, subscription_id){
        const currentVal = $('[data-cancel-btn] textarea').val();
        const cancellationValue = $('[name="cancellation_reason"]').val();
        if (cancellationValue == 'Other Reason' && currentVal == ''){
            $('[data-cancel-btn] textarea').addClass('border-danger');
            return;
        }

        $('[data-cancel-btn] textarea').removeClass('border-danger');
        const config = {
            data: {
                url: `/subscriptions/${subscription_id}/cancel`,
                method: 'POST',
                data: {
                    cancellation_reason: cancellationValue,
                    cancellation_reason_comments: currentVal,
                },
            },
          };
          app.isLoading = true;
          app.loadingEvent = 'cancel_subscription';
          rechargeAPI.post(app.rechargeDashbord.API, config)
            .then((response) => {
                if (response.data.error) {
                    app._snotifyMsg('Error', response.data.error, 'Error');
                    return false;
                } else {
                    app._getSubscriptions();
                    app.isRechargeDrawerOpen = false;
                    app.isOverlayVisible = false;
                    app._unlockScroll();
                    app.isCancel = false;
                    app._snotifyMsg('Success', 'Your Subscription has been cancelled Successfully', 'Success');
                    setTimeout(() => {
                        app.isLoading = false;
                        app.loadingEvent = null;
                    }, 500);
                }
            }).catch((error) => {
                console.log(error);
            });
    },

    /*************Eneded Script  Cancel Subscription *****************/

    /*************Start Script  Update Gift Note *****************/
    _updateGiftNote(address_id){
        const toGift = $('[data-gift-to]').val();
        const fromGift = $('[data-gift-from]').val();
        const msgGift = $('[data-gift-msg]').val();
        const config = {
            data: {
                url: `/addresses/${address_id}`,
                method: 'PUT',
                data: {
                    cart_attributes: [
                        {
                            name: 'To',
                            value: toGift,
                        },
                        {
                            name: 'Gift_Message',
                            value: msgGift,
                        },
                        {
                            name: 'From',
                            value: fromGift,
                        },
                    ],
                    note_attributes: [
                        {
                            name: 'To',
                            value: toGift,
                        },
                        {
                            name: 'Gift_Message',
                            value: msgGift,
                        },
                        {
                            name: 'From',
                            value: fromGift,
                        },
                    ],
                },
            },
        };

        app.isLoading = true;
        app.loadingEvent = 'isGiftNote';
        rechargeAPI.post(app.rechargeDashbord.API, config)
        .then((response) => {
            if (response.data.data.error) {
                app.isLoading = false;
                app.loadingEvent = null;
                app._snotifyMsg('Error', response.data.data.error, 'Error');
                return false;
            } else {
                app._getSubscriptions();
                app._getAddress(address_id);
                app.isRechargeDrawerOpen = false;
                app.isOverlayVisible = false;
                app._unlockScroll();
                app.isGiftNote = false;
                app.isGiftUpdate = false;
                app._snotifyMsg('Gift Updated', 'Gift message Updated Successfully', 'Success');
                setTimeout(() => {
                    app.isLoading = false;
                    app.loadingEvent = null;
                }, 500);
            }
        }).catch((error) => {
            console.log(error);
        });
    },
    /*************Eneded Script  Update Gift Note *****************/

    /************* Started Reactive ***************/
    _activeSubscription(event, id){
        const config = {
            data: {
                url: `/subscriptions/${id}/activate`,
                method: 'POST',
                data: {},
            },
        };
        app.isLoading = true;
        app.loadingEvent = `activate_${id}`;
        axios.post(app.rechargeDashbord.API, config)
        .then((response) => {
            if (response.data.data.error) {
                app._snotifyMsg('Error', response.data.data.error, 'Error');
                return false;
            } else {
                app._getSubscriptions();
                app._snotifyMsg('Activate', 'Your subscription successfully activated', 'Success');
                setTimeout(() => {
                    app.isLoading = false;
                    app.loadingEvent = null;
                }, 500);
            }
        }).catch((error) => {
            console.log(error);
        });
    },
    /************* Ended Reactive ***************/


    _pauseSubscriptionPicker(){
        const holidays = app.holidays;
        const holidaysDisabled = [
            function(date) {
                // must return true to disable
                return (date.getDay() === 6 || date.getDay() === 0 || date.getDay() === 1);
            },
        ];
        for (let i, e = 0, t = holidays; e < t.length; e += 1) {
            const values = {};
            values.from = t[e],
            values.to = t[e];
            holidaysDisabled.push(values);
        }
        flatpickr('#pauseSubscriptionPicker', {
            altFormat: 'M j, Y',
            altInput: true,
            disableMobile: 'true',
            dateFormat: 'Y-m-d',
            enableTime: false,
            locale: {
                firstDayOfWeek: 1, // start week on Monday
            },
            // minDate: "today",
            minDate: new Date().fp_incr(7), // 2 days from now
            onClose(selectedDates, dateStr, instance){
                $('.flatpickr-input').blur();
            },
            // enable: [
            //     function(date) {
            //         // return true to disable
            //         return (date.getDate() === 4);
            //     },
            // ],

            // disable Saturdays and Sundays and Mondays
            // disable using a function
            disable: holidaysDisabled,
        });
    },

    /**
     * Get snotifyMsg
     * @param  {Snotify Data}
     * @return {boolean}
     */
    _snotifyMsg(title, msg, type){
        if (type == 'Success'){
            app.$snotify.success(msg, title, {
                timeout: 3000,
                showProgressBar: true,
                closeOnClick: true,
                pauseOnHover: true,
                buttons: [{
                  text: 'Close',
                  bold: true,
                  closeOnClick: true,
                }],
            });
        } else if (type == 'Error'){
            app.$snotify.error(msg, title, {
                timeout: 3000,
                showProgressBar: true,
                closeOnClick: true,
                pauseOnHover: true,
                buttons: [{
                  text: 'Close',
                  bold: true,
                  closeOnClick: true,
                }],
            });
        }
    },
  },
};

export default rechargeMethods;
