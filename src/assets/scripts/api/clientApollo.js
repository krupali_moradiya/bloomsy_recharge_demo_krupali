import 'isomorphic-fetch';
import { ApolloClient } from 'apollo-client';
import { ApolloLink } from 'apollo-link';
import { createHttpLink } from 'apollo-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';

const httpLink = createHttpLink({
  uri: `https://${process.env.SLATE_STORE}/admin/api/graphql.json`,
});

const middlewareLink = new ApolloLink((operation, forward) => {
  operation.setContext({
    headers: {
      'X-Shopify-Access-Token': process.env.SLATE_PASSWORD || null,
    },
  });
  return forward(operation);
});

// use with apollo-client
const link = middlewareLink.concat(httpLink);

// create client
const clientApollo = new ApolloClient({
  link,
  addTypename: true,
  cache: new InMemoryCache(),
});

// export
export default clientApollo;
